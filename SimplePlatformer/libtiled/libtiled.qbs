import qbs 1.0

Project {
    name: "LibTiled"
    minimumQbsVersion: "1.7.1"

    Product{
        name: "libtiled"
        type: "staticlibrary"

        Depends { name: "cpp" }
        Depends { name: "Qt"; submodules: "gui"; versionAtLeast: "5.5" }

        Properties {
            condition: !qbs.toolchain.contains("msvc")
            cpp.dynamicLibraries: base.concat(["z"])
        }

        cpp.cxxLanguageVersion: "c++11"
        cpp.visibility: "minimal"
        cpp.defines: [
            "TILED_LIBRARY",
            "QT_NO_CAST_FROM_ASCII",
            "QT_NO_CAST_TO_ASCII",
            "QT_NO_URL_CAST_FROM_STRING",
            "_USE_MATH_DEFINES"
        ]

        Properties {
            condition: qbs.targetOS.contains("macos")
            cpp.cxxFlags: ["-Wno-unknown-pragmas"]
        }

        //bundle.isBundle: false
        cpp.sonamePrefix: qbs.targetOS.contains("darwin") ? "@rpath" : undefined

        Group {
            name: "Sources"
            files: [
                "compression.cpp",
                "fileformat.cpp",
                "filesystemwatcher.cpp",
                "gidmapper.cpp",
                "grouplayer.cpp",
                "hex.cpp",
                "hexagonalrenderer.cpp",
                "imagecache.cpp",
                "imagelayer.cpp",
                "imagereference.cpp",
                "isometricrenderer.cpp",
                "layer.cpp",
                "map.cpp",
                "mapformat.cpp",
                "mapobject.cpp",
                "mapreader.cpp",
                "maprenderer.cpp",
                "maptovariantconverter.cpp",
                "mapwriter.cpp",
                "object.cpp",
                "objectgroup.cpp",
                 "objecttemplate.cpp",
                "objecttemplateformat.cpp",
                "objecttypes.cpp",
                "orthogonalrenderer.cpp",
                "plugin.cpp",
                "pluginmanager.cpp",
                "properties.cpp",
                "savefile.cpp",
                "staggeredrenderer.cpp",
                "templatemanager.cpp",
                "tileanimationdriver.cpp",
                "tiled.cpp",
                "tilelayer.cpp",
                "tile.cpp",
                "tileset.cpp",
                "tilesetformat.cpp",
                "tilesetmanager.cpp",
                "varianttomapconverter.cpp",
                "wangset.cpp",
                "worldmanager.cpp",
            ]
        }

        Group{
            name: "Headers"
            files: [
                "compression.h",
                "fileformat.h",
                "filesystemwatcher.h",
                "gidmapper.h",
                "grouplayer.h",
                "hex.h",
                "hexagonalrenderer.h",
                "imagecache.h",
                "imagelayer.h",
                "imagereference.h",
                "isometricrenderer.h",
                "layer.h",
                "logginginterface.h",
                "map.h",
                "mapformat.h",
                "mapobject.h",
                "mapreader.h",
                "maprenderer.h",
                "maptovariantconverter.h",
                "mapwriter.h",
                "object.h",
                "objectgroup.h",
                "objecttemplate.h",
                "objecttemplateformat.h",
                "objecttypes.h",
                "orthogonalrenderer.h",
                "plugin.h",
                "pluginmanager.h",
                "properties.h",
                "savefile.h",
                "staggeredrenderer.h",
                "templatemanager.h",
                "tileanimationdriver.h",
                "tiled_global.h",
                "tiled.h",
                "tile.h",
                "tilelayer.h",
                "tileset.h",
                "tilesetformat.h",
                "tilesetmanager.h",
                "varianttomapconverter.h",
                "wangset.h",
                "worldmanager.h",
            ]
        }


//        Group {
//            condition: project.installHeaders
//            qbs.install: true
//            qbs.installDir: "include/tiled"
//            fileTagsFilter: "hpp"
//        }

        Export {
            Depends { name: "cpp" }
            Depends {
                name: "Qt"
                submodules: ["gui"]
            }

            cpp.includePaths: "."
        }

        Group {
            qbs.install: true
            qbs.installDir: {
                if (qbs.targetOS.contains("windows"))
                    return ""
                else if (qbs.targetOS.contains("darwin"))
                    return "Tiled.app/Contents/Frameworks"
                else
                    return "lib"
            }
            fileTagsFilter: "dynamiclibrary"
        }
    }
}
