import qbs

Project {
    minimumQbsVersion: "1.7.1"


    SubProject {
        filePath: "libtiled/libtiled.qbs"
    }

    CppApplication {
        Depends { name: "Qt.core" }
        Depends { name: "Qt.quick" }
        Depends { name: "libtiled" }

        // Additional import path used to resolve QML modules in Qt Creator's code model
        property pathList qmlImportPaths: []

        cpp.cxxLanguageVersion: "c++11"

        cpp.defines: [
            // The following define makes your compiler emit warnings if you use
            // any feature of Qt which as been marked deprecated (the exact warnings
            // depend on your compiler). Please consult the documentation of the
            // deprecated API in order to know how to port your code away from it.
            "QT_DEPRECATED_WARNINGS",

            // You can also make your code fail to compile if you use deprecated APIs.
            // In order to do so, uncomment the following line.
            // You can also select to disable deprecated APIs only up to a certain version of Qt.
            //"QT_DISABLE_DEPRECATED_BEFORE=0x060000" // disables all the APIs deprecated before Qt 6.0.0
        ]

        files: [
            "Resources/BG.png",
            "Resources/SimplePlatformMap.png",
            "Resources/SimplePlatformMap.tmx",
            "Resources/SourceImages/Object/Bush (1).png",
            "Resources/SourceImages/Object/Bush (2).png",
            "Resources/SourceImages/Object/Bush (3).png",
            "Resources/SourceImages/Object/Bush (4).png",
            "Resources/SourceImages/Object/Crate.png",
            "Resources/SourceImages/Object/Mushroom_1.png",
            "Resources/SourceImages/Object/Mushroom_2.png",
            "Resources/SourceImages/Object/Sign_1.png",
            "Resources/SourceImages/Object/Sign_2.png",
            "Resources/SourceImages/Object/Stone.png",
            "Resources/SourceImages/Object/Tree_1.png",
            "Resources/SourceImages/Object/Tree_2.png",
            "Resources/SourceImages/Object/Tree_3.png",
            "Resources/SourceImages/Tiles.png",
            "Resources/SourceImages/Tiles/1.png",
            "Resources/SourceImages/Tiles/10.png",
            "Resources/SourceImages/Tiles/11.png",
            "Resources/SourceImages/Tiles/12.png",
            "Resources/SourceImages/Tiles/13.png",
            "Resources/SourceImages/Tiles/14.png",
            "Resources/SourceImages/Tiles/15.png",
            "Resources/SourceImages/Tiles/16.png",
            "Resources/SourceImages/Tiles/17.png",
            "Resources/SourceImages/Tiles/18.png",
            "Resources/SourceImages/Tiles/2.png",
            "Resources/SourceImages/Tiles/3.png",
            "Resources/SourceImages/Tiles/4.png",
            "Resources/SourceImages/Tiles/5.png",
            "Resources/SourceImages/Tiles/6.png",
            "Resources/SourceImages/Tiles/7.png",
            "Resources/SourceImages/Tiles/8.png",
            "Resources/SourceImages/Tiles/9.png",
            "Resources/Tiles.tmx",
            "main.cpp",
            "qml.qrc",
            "viewer.cpp",
            "viewer.h",
        ]

        Group {     // Properties for the produced executable
            name: "simpleplatformer"
            fileTagsFilter: "application"
            qbs.install: true
        }

        Group {
            name: "QML Files"
            files: ["*.qml"]
        }

    }
}
