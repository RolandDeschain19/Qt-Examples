import QtQuick 2.9
import QtQuick.Window 2.3
import Viewer 1.0

Window {
    id: rootWindow
    visible: true
    maximumHeight: 750
    maximumWidth: 1000
    minimumHeight:  750
    minimumWidth: 1000
    width: 1000
    height: 750
    title: qsTr("TMX QML Viewer")

    Image {
        source: "Resources/BG.png"
        anchors.fill: parent
    }
    Viewer{
        anchors.fill: parent
        rightAligned: true
        antialiasing: true
        width: parent.width
        height: parent.height
    }

}

