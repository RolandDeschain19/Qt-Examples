#include <QtTest>
#include <QCoreApplication>

// add necessary includes here

class UnitTestOne : public QObject
{
    Q_OBJECT

public:
    UnitTestOne();
    ~UnitTestOne();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

UnitTestOne::UnitTestOne()
{

}

UnitTestOne::~UnitTestOne()
{

}

void UnitTestOne::initTestCase()
{

}

void UnitTestOne::cleanupTestCase()
{

}

void UnitTestOne::test_case1()
{

}

QTEST_MAIN(UnitTestOne)

#include "tst_unittestone.moc"
