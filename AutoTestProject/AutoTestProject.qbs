import qbs

CppApplication {
    Depends { name: "Qt.testlib" }
    Depends { name: "Qt.gui" }
    files: [
        "tst_unittestone.cpp"
    ]
}
